package com.example.haiphan.chatclientandroid;

import android.util.Log;

import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by HaiPhan on 9/20/2015.
 */
public class MessageSender implements Runnable {
    private Socket socket;
    private String msgOut;
    private PrintWriter out;

    public MessageSender(Socket socket, String msgOut) {
        this.socket = socket;
        this.msgOut = msgOut;
    }

    @Override
    public void run() {
        try {
            out = new PrintWriter(socket.getOutputStream());
            out.println(msgOut);
            out.flush();
        } catch (Exception e) {
            Log.e("TCP", "C:	Error", e);
        }
    }
}
