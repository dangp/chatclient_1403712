package com.example.haiphan.chatclientandroid;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by HaiPhan on 9/19/2015.
 */
public class MessageReceiver implements Runnable {
    private Handler uiHandler;
    private Socket socket;
    private String HOST = "10.112.222.239";
    private int PORT = 12345;
    private BufferedReader br;


    public MessageReceiver(Handler uiHandler, Socket socket) {
        this.socket = socket;
        this.uiHandler = uiHandler;
    }


    @Override
    public void run() {
        try {
//            InetAddress serverAddr = InetAddress.getByName(HOST);
//            socket = new Socket(serverAddr, PORT);
            socket.connect(new InetSocketAddress(HOST, PORT));
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while (true) {
                String chatMessage = br.readLine();
                Message msg = uiHandler.obtainMessage();
                msg.what = 0;
                msg.obj = chatMessage;
                uiHandler.sendMessage(msg);
            }
        } catch (Exception e) {
            Log.e("TCP", "C:	Error", e);
        } finally {
            try {
                socket.close();
            } catch (Exception e) {
                Log.e("TCP", "C:	Error", e);
            }
        }
    }
}
