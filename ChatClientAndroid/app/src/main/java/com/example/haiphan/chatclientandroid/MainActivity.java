package com.example.haiphan.chatclientandroid;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.net.Socket;

public class MainActivity extends Activity {
    Socket socket = new Socket();
    private Handler uiHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 0) {
                TextView textView = (TextView) findViewById(R.id.textView);
                textView.append((String) msg.obj + "\n");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button) findViewById(R.id.button);
        final EditText editText = (EditText) findViewById(R.id.editText);

        final MessageReceiver msgReceiver = new MessageReceiver(uiHandler, socket);
        Thread t = new Thread(msgReceiver);
        t.start();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msgOut = editText.getText().toString();
                MessageSender msgSender = new MessageSender(socket, msgOut);
                Thread t = new Thread(msgSender);
                t.start();
                editText.getText().clear();

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
